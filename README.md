# Instant tor relay

Aim of this project is to quickly setup a tor relay.

## Quickstart

1. Install Ansible dependencies, roles from [my Gitlab](https://gitlab.com/stemid-ansible/roles) and modules from Ansible's community collections:  ``ansible-galaxy install -r requirements.yml; ansible-galaxy collection install -r requirements.yml``
2. Install the python dependencies, for route53, linode, digitalocean: ``pip install --user boto linode_api4``
3. Install [my own fork of dopy for Python 3](https://gitlab.com/stemid/dopy3): ``pip install --user https://gitlab.com/stemid/dopy3``
3. Setup the necessary access tokens, Linode, AWS and so forth
4. Run linode.yml to create the VPS (Because of [Github issue #1764](https://github.com/ansible-collections/community.general/issues/1764) you must ``export LINODE_ACCESS_TOKEN=your.token`` first)
6. Add the linode instance to your ssh config, linode.yml should print its IP
7. Run bootstrap.yml to prep the VPS with useful tools
8. Run site.yml to install and configure tor
9. Set ``state: absent`` in ``inventory/default/group_vars/all.yml`` for your linode instance and re-run linode.yml and route53.yml to delete all instances and their DNS records.

## Settings

Copy the default environment to avoid conflicts when you try to update this git repo. Name your environment whatever you want, I'm naming mine linode arbitrarily.

    cp -r inventory/default inventory/linode

Read the ``group_vars/all.yml`` file for settings but here is the gist.

* Define linode instances in ``linode_instances`` list
* and DO instances in ``digitalocean_instances`` list.
* See the examples for how.

## Create instances and their DNS

    export LINODE_ACCESS_TOKEN=deadbeef
    ansible-playbook -i inventory/linode/linode.yml linode.yml
    export DIGITALOCEAN_ACCESS_TOKEN=deadbeef2
    ansible-playbook -i inventory/linode/linode.yml digitalocean.yml

Both these playbooks will output the name and IP of the instances that are created. Write those down in your ssh client config before you can continue.

## Setup software

    ansible-playbook -i inventory/linode/linode.yml bootstrap.yml
    ansible-playbook -i inventory/linode/linode.yml site.yml

### Digitalocean

Their inventory setup is a bit different, first generate one with ``do-ansible-inventory``.

    do-ansible-inventory --group-by-tag --tags=torserver --ssh-user=root > inventory/linode/digitalocean.ini
    ansible-playbook -i inventory/linode/digitalocean.ini bootstrap.yml
    ansible-playbook -i inventory/linode/digitalocean.ini site.yml

